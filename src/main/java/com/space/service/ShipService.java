package com.space.service;

import com.space.exception.BadRequestException;
import com.space.exception.ShipNotFoundException;
import com.space.model.Ship;
import com.space.model.ShipType;
import com.space.repository.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ShipService {

    @Autowired
    ShipRepository repository;

    public Specification<Ship> filterByName(String name) {
        return (Specification<Ship>) (root, query, cb) -> (name == null ? null : cb.like(root.get("name"), "%" + name + "%"));
    }

    public Specification<Ship> filterByPlanet(String planet) {
        return (Specification<Ship>) (root, query, cb) -> (planet == null ? null : cb.like(root.get("planet"), "%" + planet + "%"));
    }


    public Specification<Ship> filterByShipType(ShipType type) {
        return (Specification<Ship>) (root, query, cb) -> (type == null ? null : cb.equal(root.get("shipType"), type));
    }


    public Specification<Ship> filterByProdDate(Long after, Long before) {
        return (Specification<Ship>) (root, query, criteriaBuilder) -> {
            if (after == null && before == null) return null;
            if (after == null) {
                Date date = new Date(before);
                return criteriaBuilder.lessThanOrEqualTo(root.get("prodDate"), date);
            }
            if (before == null) {
                Date date = new Date(after);
                return criteriaBuilder.greaterThanOrEqualTo(root.get("prodDate"), date);
            }
            Date dateBefore = new Date(before);
            Date dateAfter = new Date(after);
            return criteriaBuilder.between(root.get("prodDate"), dateAfter, dateBefore);


        };
    }

    public Specification<Ship> filterByUsed(Boolean isUsed) {

        return  (root, query, criteriaBuilder) -> {
            if (isUsed == null) return null;
            if (isUsed) return criteriaBuilder.isTrue(root.get("isUsed"));
            else return criteriaBuilder.isFalse(root.get("isUsed"));

        };
    }

    public Specification<Ship> filterBySpeed(Double min, Double max) {

        if (min == null && max == null)
            return null;

        if (min == null) {
            return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("speed"), max);
        }
        if (max == null) {
            return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("speed"), min);
        }
        return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get("speed"), min, max);
    }

    public Specification<Ship> filterByCrewSize(Integer min, Integer max) {
        if (min == null && max == null) return null;
        if (min == null) {
            return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("crewSize"), max);
        }
        if (max == null) {
            return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("crewSize"), min);
        }
        return (Specification<Ship>) (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get("crewSize"), min, max);
    }

    public Specification<Ship> filterByRating(Double min, Double max) {

        return (root, query, cb) -> {
            if (min == null && max == null)
                return null;
            if (min == null)
                return cb.lessThanOrEqualTo(root.get("rating"), max);
            if (max == null)
                return cb.greaterThanOrEqualTo(root.get("rating"), min);

            return cb.between(root.get("rating"), min, max);
        };
    }


    public Page<Ship> getAllShips(Specification<Ship> specification, Pageable pageable) {
        return repository.findAll(specification, pageable);
    }


    public List<Ship> gelAllShips(Specification<Ship> specification) {
        return repository.findAll(specification);
    }

    public Ship create(Ship ship) {
        if (ship.getName() == null
                || ship.getPlanet() == null
                || ship.getShipType() == null
                || ship.getProdDate() == null
                || ship.getSpeed() == null
                || ship.getCrewSize() == null)
            throw new BadRequestException("One of Ship params is null");

        checkParams(ship);

        if (ship.getUsed() == null)
            ship.setUsed(false);

        Double rating = calculateRating(ship);
        ship.setRating(rating);

        return repository.saveAndFlush(ship);
    }

    private Double calculateRating(Ship ship) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(ship.getProdDate());
        int year = cal.get(Calendar.YEAR);


        BigDecimal raiting = new BigDecimal((80 * ship.getSpeed() * (ship.getUsed() ? 0.5 : 1)) / (3019 - year + 1));

        raiting = raiting.setScale(2, RoundingMode.HALF_UP);
        return raiting.doubleValue();
    }



    public Ship getShipById(Long id) {
        if (!repository.existsById(id))
            throw new ShipNotFoundException("Ship not found");
        return repository.findById(id).get();
    }



    public void deleteById(Long id) {
        if (repository.existsById(id))
            repository.deleteById(id);
        else throw new ShipNotFoundException("Ship not found");
    }

    public Ship editShip(Long id, Ship ship) {

        checkParams(ship);

        if (!repository.existsById(id))
            throw new ShipNotFoundException("Ship not found");

        Ship editedShip = repository.findById(id).get();

        if (ship.getName() != null)
            editedShip.setName(ship.getName());

        if (ship.getPlanet() != null)
            editedShip.setPlanet(ship.getPlanet());

        if (ship.getShipType() != null)
            editedShip.setShipType(ship.getShipType());

        if (ship.getProdDate() != null)
            editedShip.setProdDate(ship.getProdDate());

        if (ship.getSpeed() != null)
            editedShip.setSpeed(ship.getSpeed());

        if (ship.getUsed() != null)
            editedShip.setUsed(ship.getUsed());

        if (ship.getCrewSize() != null)
            editedShip.setCrewSize(ship.getCrewSize());

        Double rating = calculateRating(editedShip);
        editedShip.setRating(rating);

        return repository.save(editedShip);
    }

    public Long checkId(String id) {
        if (id == null || id.equals("0") || id.equals(""))
            throw new BadRequestException("Wrong ID");

        try {
            Long longid = Long.parseLong(id);
            return longid;
        } catch (NumberFormatException e) {
            throw new BadRequestException("ID must be Number", e);
        }
    }


    private void checkParams(Ship ship) {

        if (ship.getName() != null && (ship.getName().length() < 1 || ship.getName().length() > 50))
            throw new BadRequestException("Incorrect Ship.name");

        if (ship.getPlanet() != null && (ship.getPlanet().length() < 1 || ship.getPlanet().length() > 50))
            throw new BadRequestException("Incorrect Ship.planet");

        if (ship.getCrewSize() != null && (ship.getCrewSize() < 1 || ship.getCrewSize() > 9999))
            throw new BadRequestException("Incorrect Ship.crewSize");

        if (ship.getSpeed() != null && (ship.getSpeed() < 0.01D || ship.getSpeed() > 0.99D))
            throw new BadRequestException("Incorrect Ship.speed");

        if (ship.getProdDate() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(ship.getProdDate());
            if (cal.get(Calendar.YEAR) < 2800 || cal.get(Calendar.YEAR) > 3019)
                throw new BadRequestException("Incorrect Ship.date");
        }
    }
}

