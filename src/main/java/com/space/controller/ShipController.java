package com.space.controller;

import com.space.model.Ship;
import com.space.model.ShipType;
import com.space.service.ShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class ShipController {
    @Autowired
    private ShipService service;

    @GetMapping(value = "/ships")
    @ResponseStatus(HttpStatus.OK)
    public List<Ship> getAll(

            @RequestParam(value = "pageNumber", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "3") Integer limit,
            @RequestParam(value = "order", required = false, defaultValue = "ID") ShipOrder orderBy,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "planet", required = false) String planet,
            @RequestParam(name = "shipType", required = false) ShipType shipType,
            @RequestParam(name = "after", required = false) Long after,
            @RequestParam(name = "before", required = false) Long before,
            @RequestParam(name = "isUsed", required = false) Boolean isUsed,
            @RequestParam(name = "minSpeed", required = false) Double minSpeed,
            @RequestParam(name = "maxSpeed", required = false) Double maxSpeed,
            @RequestParam(name = "minCrewSize", required = false) Integer minCrewSize,
            @RequestParam(name = "maxCrewSize", required = false) Integer maxCrewSize,
            @RequestParam(name = "minRating", required = false) Double minRating,
            @RequestParam(name = "maxRating", required = false) Double maxRating

    ) {
        Pageable pageable = PageRequest.of(page, limit, Sort.by(orderBy.getFieldName()));


        return service.getAllShips(
                Specification.where(service.filterByName(name))
                        .and(service.filterByPlanet(planet))
                        .and(service.filterByShipType(shipType))
                        .and(service.filterByProdDate(after, before))
                        .and(service.filterByUsed(isUsed))
                        .and(service.filterBySpeed(minSpeed, maxSpeed))
                        .and(service.filterByCrewSize(minCrewSize, maxCrewSize))
                        .and(service.filterByRating(minRating, maxRating)), pageable)
                .getContent();
    }

    @RequestMapping(value = "/ships/count", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Integer getCount(@RequestParam(value = "name", required = false) String name,
                            @RequestParam(value = "planet", required = false) String planet,
                            @RequestParam(value = "shipType", required = false) ShipType shipType,
                            @RequestParam(value = "after", required = false) Long after,
                            @RequestParam(value = "before", required = false) Long before,
                            @RequestParam(value = "isUsed", required = false) Boolean isUsed,
                            @RequestParam(value = "minSpeed", required = false) Double minSpeed,
                            @RequestParam(value = "maxSpeed", required = false) Double maxSpeed,
                            @RequestParam(value = "minCrewSize", required = false) Integer minCrewSize,
                            @RequestParam(value = "maxCrewSize", required = false) Integer maxCrewSize,
                            @RequestParam(value = "minRating", required = false) Double minRating,
                            @RequestParam(value = "maxRating", required = false) Double maxRating) {

        return service.gelAllShips(
                Specification.where(service.filterByName(name)
                        .and(service.filterByPlanet(planet)))
                        .and(service.filterByShipType(shipType))
                        .and(service.filterByProdDate(after, before))
                        .and(service.filterByUsed(isUsed))
                        .and(service.filterBySpeed(minSpeed, maxSpeed))
                        .and(service.filterByCrewSize(minCrewSize, maxCrewSize))
                        .and(service.filterByRating(minRating, maxRating)))
                .size();
    }

    @PostMapping("/ships")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Ship create(@RequestBody Ship ship) {
       return service.create(ship);
    }

    @GetMapping("/ships/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Ship getShipById(@PathVariable String id) {
        Long longId = service.checkId(id);
        return service.getShipById(longId);
    }

    @DeleteMapping(value = "/ships/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteShip(@PathVariable(value = "id") String id) {
        Long longId = service.checkId(id);
        service.deleteById(longId);
    }

    @PostMapping(value = "/ships/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Ship editShip(@PathVariable(value = "id") String id, @RequestBody Ship ship) {

        Long longId = service.checkId(id);

        return service.editShip(longId, ship);
    }

}